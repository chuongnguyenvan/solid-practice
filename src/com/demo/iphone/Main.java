package com.demo.iphone;

import com.demo.solid.OCP.BlueToothConnection;

/**
 * Created by chuongnguyen on 3/10/2016.
 */
public class Main {
    public static void main(String[] args){
        IPhone6 iphone = new IPhone6();
        iphone.displayPowerState();
        iphone.powerON();
        iphone.openPhone();
        iphone.doConnect(new BlueToothConnection());




    }
}
