package com.demo.iphone;
import com.demo.solid.ISP.ICall;
import com.demo.solid.ISP.ISend;
import com.demo.solid.ISP.ITrackID;
import com.demo.solid.LSP.Phone;
import com.demo.solid.OCP.BlueToothConnection;
import com.demo.solid.OCP.Connection;
import com.demo.solid.SRP.ConnectionManager;
import com.demo.solid.SRP.Lock;
import com.demo.solid.SRP.Power;
import com.demo.solid.SRP.Power.PowerState;
/**
 * Created by chuongnguyen on 3/10/2016.
 */
public class IPhone6 extends Phone implements ICall,ISend,ITrackID{
    private PowerState powerState = Power.PowerState.OFF;
    private Power power;
    private Lock lock;
    private ConnectionManager connectionManager;
    public IPhone6(){
        power = new Power(this);
        lock = new Lock(this);
        connectionManager = new ConnectionManager(this);

    }
    public PowerState getPowerState(){
        return this.powerState;
    }
    public void setPowerState(PowerState powerState){
        this.powerState = powerState;
    }
    public void displayPowerState(){
        System.out.println(powerState);
    }
    public void powerOff(){
        this.power.turnOFF();
    }
    public void powerON(){
        this.power.turnON();
    }
    public void openPhone(){
        this.lock.unlock();
    }
    public void doConnect(Connection iconnect){
        connectionManager.connect(iconnect);
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public String getMaterial() {
        return this.material;
    }

    @Override
    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public void call() {

    }

    @Override
    public void send() {

    }

    @Override
    public void trackID() {

    }
}
