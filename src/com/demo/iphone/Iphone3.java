package com.demo.iphone;

import com.demo.solid.ISP.ICall;
import com.demo.solid.ISP.ISend;
import com.demo.solid.LSP.Phone;

/**
 * Created by chuongnguyen on 3/10/2016.
 */
public class Iphone3 extends Phone implements ICall,ISend{
    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public void setHeight(int height) {

    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public void setWidth(int width) {

    }

    @Override
    public String getMaterial() {
        return null;
    }

    @Override
    public void setMaterial(String material) {

    }

    @Override
    public void call() {

    }

    @Override
    public void send() {

    }
}
