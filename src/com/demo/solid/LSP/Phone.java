package com.demo.solid.LSP;

/**
 * Created by chuongnguyen on 3/10/2016.
 */
public abstract class Phone {
    public int height;
    public int width;
    public String material;
    public abstract int getHeight();
    public abstract void setHeight(int height);
    public abstract int getWidth();
    public abstract void setWidth(int width);
    public abstract String getMaterial();
    public abstract void setMaterial(String material);

}
