package com.demo.solid.SRP;

import com.demo.iphone.IPhone6;

/**
 * Created by chuongnguyen on 3/10/2016.
 */
public class Lock {
    private IPhone6 IPhone;

    public Lock(IPhone6 IPhone){
        this.IPhone = IPhone;
    }

    public void unlock(){
        if(IPhone.getPowerState() == Power.PowerState.ON){
            System.out.println("Slide to unclock");
        }
    }

    public void lock(){
        if(IPhone.getPowerState() == Power.PowerState.OFF){
            System.out.println("You can't");
        }
    }
}
