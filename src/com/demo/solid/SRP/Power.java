package com.demo.solid.SRP;

import com.demo.iphone.IPhone6;

/**
 * Created by chuongnguyen on 3/10/2016.
 */
public class Power {
    public enum PowerState {
        ON,OFF
    }

    private IPhone6 IPhone;

    public Power(IPhone6 IPhone){
        this.IPhone = IPhone;
    }

    public void turnOFF(){
        if(IPhone != null && IPhone.getPowerState() == PowerState.ON ){
            IPhone.setPowerState(PowerState.OFF);
        }
    }
    public void turnON(){
        if(IPhone != null && IPhone.getPowerState() == PowerState.OFF ){
            IPhone.setPowerState(PowerState.ON);
        }
    }



}
