package com.demo.solid.SRP;
import com.demo.iphone.IPhone6;
import com.demo.solid.OCP.Connection;

/**
 * Created by chuongnguyen on 3/10/2016.
 */
public class ConnectionManager {
    private IPhone6 IPhone;

    public ConnectionManager(IPhone6 IPhone){
        this.IPhone = IPhone;
    }
    public void connect(Connection iconnect){
        iconnect.doConnect();
    }
}



