package com.demo.solid.DIP;

/**
 * Created by chuongnguyen on 3/10/2016.
 */
public class Setting {
    INotification iNotification;

    public Setting(INotification iNotification){
        this.iNotification = iNotification;
    }

    public void sendNotification(){
        iNotification.callNotification();
    }
}
