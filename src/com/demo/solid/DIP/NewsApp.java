package com.demo.solid.DIP;

/**
 * Created by chuongnguyen on 3/10/2016.
 */
public class NewsApp implements INotification{
    @Override
    public void callNotification() {
        System.out.println("NewsApp shows the hot news");
    }
}
