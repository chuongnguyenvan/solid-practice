package com.demo.solid.DIP;

/**
 * Created by chuongnguyen on 3/10/2016.
 */
public class GameApp implements INotification {
    @Override
    public void callNotification() {
        System.out.println("GameApp show the hot event");
    }
}
