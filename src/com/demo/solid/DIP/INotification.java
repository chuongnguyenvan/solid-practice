package com.demo.solid.DIP;

/**
 * Created by chuongnguyen on 3/10/2016.
 */
public interface INotification {
    void callNotification();
}
