package com.demo.solid.OCP;

/**
 * Created by chuongnguyen on 3/10/2016.
 */
public class BlueToothConnection extends Connection{
    @Override
    public void doConnect() {
        System.out.println("Connect with bluetooh");
    }
}
