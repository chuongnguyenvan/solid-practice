package com.demo.solid.ISP;

/**
 * Created by chuongnguyen on 3/10/2016.
 */
public interface ICall {
    void call();
}
